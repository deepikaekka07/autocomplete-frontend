var countryList = [];
var inputArray = [];
var selectedJsonList = [];
var json ;
function loadJSON() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
       json = JSON.parse(this.responseText);
       console.log(json);
       getCountryList(json);
      }
    };
    xhttp.open("GET", "https://restcountries.eu/rest/v2/all", true);
    xhttp.send();
  }

  function getCountryList(obj) {
    var i;
    for(i=0; i<obj.length; i++){
        countryList.push(obj[i].name);
    }
  }

  loadJSON();

  var currentActivePos = -1;
  var event =  document.getElementById("searchCountry");
  if(event){
    var isFound = false;
  event.addEventListener("input", function(){
    var a, b, i, val = this.value;
    closeAllLists();
    if (!val) {
        return false;
    }
    currentActivePos = -1;
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    this.parentNode.appendChild(a);
    for (i = 0; i < countryList.length; i++) {
        if (countryList[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            isFound = true;
          b = document.createElement("DIV");
          b.innerHTML = "<strong>" + countryList[i].substr(0, val.length) + "</strong>";
          b.innerHTML += countryList[i].substr(val.length);
          b.innerHTML += "<input type='hidden' value='" + countryList[i] + "'>";
          b.addEventListener("click", function(e) {
              event.value = this.getElementsByTagName("input")[0].value;
              createAnotherInput(event);
              event.value = "";
              closeAllLists();
          });
          a.appendChild(b);
        }

    // document.getElementById("displayJSON").innerHTML += "bla bla";
    }
    if(!isFound){
        a.parentElement.removeChild(this);
    }

});


event.addEventListener("keydown", function(e){
    var el = document.getElementById(this.id + "autocomplete-list");
      if (el) {
        el = el.getElementsByTagName("div");
        clearActiveDiv(el);
      }
      //down arrow
      if(el != null ){
        if(e.keyCode == 40) {
            currentActivePos++;
        } else if (e.keyCode == 38) // up arrow
        {
            currentActivePos--;
        }else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentActivePos > -1) {
              if (el) el[currentActivePos].click();
            } 
            createAnotherInput(event);
        }

        if(currentActivePos == el.length){
            currentActivePos = el.length - 1;
        }
        if( currentActivePos < 0) {
            currentActivePos = 0;
        }
        if(el.length !=0) el[currentActivePos].classList.add("active");
      }
});
}

function createAnotherInput(event) {
 if(event.value != "" && event.value != undefined){
    var d = document.getElementsByClassName("selected");
    for(var i=inputArray.length-1; i>=0; i--){
        d[i].outerHTML = "";
    } 
    if(!inputArray.includes(event.value)){
        inputArray.push(event.value);
        addInJSONList(event.value);
    }
    for(var i=0; i<inputArray.length; i++){
        var str = '<span class="selected" onclick="removeInput(event)" value="' + inputArray[i] + '">' + inputArray[i]  +'<i class="fa fa-close"></i></span>'; 
        event.parentElement.insertAdjacentHTML("beforebegin", str) ;
    }
    
    console.log(event);
 }
 
}

function removeInput(ev){
    var elem = ev.currentTarget.innerText;
    ev.currentTarget.outerHTML = "";
    inputArray.pop(elem);
    removeFromJSONList(elem);
    console.log("remove"+ev);
}



function clearActiveDiv(el) {
    for(var i = 0; i < el.length ; i++){
        el[i].classList.remove("active");
    }
}

function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != event) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

document.addEventListener("click", function (e) {
    
    // if(event!=null) createAnotherInput(event);
    closeAllLists(e.target);
    currentActivePos = -1;
});

function addInJSONList(country) {
    for(var i=0; i<json.length; i++) {
        if(json[i].name.toUpperCase() == country.toUpperCase()){
            selectedJsonList.push(json[i]);
        }
    }
  
  document.getElementById("displayJSON").innerHTML = JSON.stringify(selectedJsonList);
}

function removeFromJSONList(country) {
    var list = selectedJsonList;
    for(var i=selectedJsonList.length-1; i>=0; i--) {
        if(selectedJsonList[i].name.toUpperCase() == country.toUpperCase()){
            list.splice(i,1);
        }
    }
    selectedJsonList = list;
    document.getElementById("displayJSON").innerHTML = JSON.stringify(selectedJsonList);
}
